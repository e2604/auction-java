package com.company.state;

import com.company.Auction;

public abstract class State {
    public abstract void startSale(Auction auction);
    public abstract void raisePrice(Auction auction);
    public abstract void withdraw(Auction auction);
    public abstract void giveToTheWinner(Auction auction);
}
