package com.company;

import com.company.state.State;

public class Auction {

    private String id;
    private String name;
    private String stateName;
    private transient State state;
    private int price;
    private String honoraryCode;

    public void startSale(){
        state.startSale(this);
    }

    public void raisePrice(){
        state.raisePrice(this);
    }

    public void withdraw(){
        state.withdraw(this);
    }

    public void giveToTheWinner(){
        state.giveToTheWinner(this);
    }

    public String getId() {
        return id;
    }


    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }


    public void setState(State state) {
        this.state = state;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    public void setHonoraryCode(String honoraryCode) {
        this.honoraryCode = honoraryCode;
    }

    @Override
    public String toString() {
        return  "-------+----------+-------------------------+-----------------+--------------+--------------------------\n"+
                "   #   |    ID    |       Наименование      |      Статус     |     Цена     |          code    \n" +
                "-------+----------+-------------------------+-----------------+--------------+--------------------------\n"+
                "       |   " + id  +
                " |         " + name +
                "        |       " + stateName  +
                "     |      " + price +
                "     |          " + honoraryCode;
    }
}
