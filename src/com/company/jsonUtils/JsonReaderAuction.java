package com.company.jsonUtils;

import com.company.Auction;
import com.company.state.ForSale;
import com.company.state.InStock;
import com.company.state.Sold;
import com.google.gson.Gson;

import java.io.FileReader;
import java.io.IOException;

public class JsonReaderAuction {
    private final String path;
    private final Gson gson;

    public JsonReaderAuction(String path, Gson gson) {
        this.path = path;
        this.gson = gson;
    }

    public Auction[] getGoods() throws IOException{
        FileReader reader = new FileReader(path);
        Auction[] auctions = gson.fromJson(reader, Auction[].class);
        reader.close();
        initStates(auctions);
        return auctions;
    }

    private  void initStates(Auction[] auctions){
        for(Auction auction : auctions){
            switch (auction.getStateName()){
                case "stock":
                    auction.setState(new InStock());
                    break;
                case "sale":
                    auction.setState(new ForSale());
                    break;
                case "sold":
                    auction.setState(new Sold());
                    break;
            }

        }
    }
}